package test.java;

import main.java.logic.FirstUniqueChar;
import org.junit.Assert;
import org.junit.Test;

public class FirstUniqueCharTest {

    @Test
    public void getFirstUniqueTest() {

        FirstUniqueChar firstUniqueChar = new FirstUniqueChar("ana nu are merisoare");
        String firstUniqueResult = firstUniqueChar.getFirstUnique();

        Assert.assertEquals("u", firstUniqueResult);
    }

    @Test
    public void getFirstUniqueNotFindTest() {
        FirstUniqueChar firstUniqueChar = new FirstUniqueChar("ana nu are meremu");
        String firstUniqueResult = firstUniqueChar.getFirstUnique();

        Assert.assertEquals("!!! No unique elements !!!", firstUniqueResult);
    }
}
