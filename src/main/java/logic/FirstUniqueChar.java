/*
Literele mari sunt diferite de cele mici. Spatiile nu le iau in considerare si nici semnele de punctuatie.
[ "," , ".", "!"]
*/

package main.java.logic;

import java.util.Arrays;

public class FirstUniqueChar {

    private String word;

    public FirstUniqueChar(String word) {
        this.word = word;
    }

    //next method will return the first unique element
    public String getFirstUnique() {

        //next 2 lines creates an array of string and contains all elements of word without spaces and splited by ""
        String wordNoSpaces = this.word.replaceAll("\\s+", ""); //remove white spaces
        String[] wordToArray = wordNoSpaces.split("");

        //now is looking for first character (which is actually treated like a String)
        // that is not repeating from the beging of array till the end more than one times

        int count;
        for (int i = 0; i < wordToArray.length; i++) {
            count = 0;
            for (int j = 0; j < wordToArray.length; j++) {
                if (wordToArray[i].equals(wordToArray[j])) {    //nu a functionat cu == si e ciudat
                    count++;
                    if (count > 1) // j starts from 0 everytime so at position i its a match, that's why count is at least 1
                        break;
                }
            }
            if (count == 1)
                return wordToArray[i];
        }

        return "!!! No unique elements !!!";
    }
}
